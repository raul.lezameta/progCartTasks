function createCanvas(width, height, className) {

  var canvas = document.createElement('canvas');

  canvas.width = width;
  canvas.height = height;
  canvas.className = className;

  document.getElementById('wrapper').appendChild(canvas);

  return canvas;
}

function createDrawCanvas(width, height, zoom, moveCallback, clickCallback) {

  zoom = zoom || 1;

  var canvas = createCanvas(width * zoom, height * zoom, zoom > 1 ? "zoom" : "");
  var context = canvas.getContext("2d");

  context.drawPixel = function (x, y, color) {
    var xr = Math.round(x);
    var yr = Math.round(y);
    if (xr != x || yr != y) {
      console.warn("Warning: drawPixel should be called with integer (rounded) coordinates. Coordinates given: " + x + "," + y);
    }
    color = color || "#000000";
    this.save();
    this.fillStyle = color;
    this.fillRect(xr * zoom, yr * zoom, zoom, zoom);
    this.restore();
  };
  context.drawStatus = function (text) {
    this.save();
    this.fillStyle = "#000000";
    var textWidth = this.measureText(text).width;
    this.fillText(text, width * zoom - textWidth - 3, height * zoom - 3);
    this.restore();
  };
  context.clear = function () {
    this.save();
    this.fillStyle = "#ffffff";
    this.fillRect(0, 0, width * zoom, height * zoom);
    this.restore();
  };

  canvas.addEventListener("click", function (event) {
    var relX = event.clientX - this.offsetLeft;
    var relY = event.clientY - this.offsetTop;
    relX = Math.ceil(relX / zoom) - 1;
    relY = Math.ceil(relY / zoom) - 1;
    clickCallback.call(context, relX - 1, relY - 1);
  });
  canvas.addEventListener("mousemove", function (event) {
    var relX = event.clientX - this.offsetLeft;
    var relY = event.clientY - this.offsetTop;
    relX = Math.ceil(relX / zoom) - 1;
    relY = Math.ceil(relY / zoom) - 1;
    // move point a bit so it is not directly under the mouse
    moveCallback.call(context, relX - 1, relY - 1);
  });
  return context;
}

var SIZE = 150;

var canvas_zoom = createDrawCanvas(SIZE, SIZE, 5, moveCallback, clickCallback);
var canvas = [];
for (var i = 0; i < lineFunctions.length; i++) {
  canvas[i] = createDrawCanvas(SIZE, SIZE, 1, moveCallback, clickCallback);
  canvas[i].lineFunc = lineFunctions[i];
}

var activeCanvas = null;

function setActiveCanvas(canvas) {
  if (canvas != activeCanvas) {
    if (activeCanvas) activeCanvas.canvas.classList.remove("active");
    activeCanvas = canvas;
    if (activeCanvas) activeCanvas.canvas.classList.add("active");
  }
}

setActiveCanvas(canvas[0]);

var startX = 50;
var startY = 50;

var drawPixel = function () { };

function setCanvas(canvas) {
  drawPixel = function (x, y, color) {
    canvas.drawPixel(x, y, color);
    if (canvas == activeCanvas) {
      canvas_zoom.drawPixel(x, y, color);
    }
  }
}

function clear() {
  for (var i = 0; i < canvas.length; i++) {
    canvas[i].clear();
  }
  canvas_zoom.clear();
}

function drawCross(x, y, radius, color) {
  drawPixel(x - radius, y, color);
  drawPixel(x + radius, y, color);
  drawPixel(x, y - radius, color);
  drawPixel(x, y + radius, color);
}


function moveCallback(x, y) {
  clear();

  if (this != canvas_zoom) {
    setActiveCanvas(this);
  }

  for (var i = 0; i < canvas.length; i++) {

    setCanvas(canvas[i]);

    drawCross(startX, startY, 1, "#ff0000");
    drawCross(x, y, 1, "#00ff00");

    var startTime = window.performance.now();
    // invoke actual line-drawing function - get it by name from the global object
    window[canvas[i].lineFunc](startX, startY, x, y);
    var endTime = window.performance.now();
    var duration = Math.round((endTime - startTime) * 1000);
    // canvas[i].drawStatus(duration + " µs");
    canvas[i].drawStatus(lineFunctions[i]);
  }
}

function clickCallback(x, y) {
  startX = x;
  startY = y;
}
