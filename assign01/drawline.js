// if you want to add another line drawing function,
// add its name to this array and it will be drawn


var lineFunctions = ['drawLine', 'drawLineDashed', 'drawLineAntiAliasing'];

// User can input his own pattern

var patternString = prompt('Put in your pattern (dash, gap, ...)').split(',');
var pattern = [];

patternString.forEach((ptrn) => {
  pattern.push(parseInt(ptrn, 10));
});

function drawLine(x1, y1, x2, y2) {
  // TODO: remove dummy code and implement drawing a line

  // bonus point: experiment with anti-aliasing, using drawPixel(x, y, color)

  var dx = x2 - x1;
  var dy = y2 - y1;

  var length = Math.max(Math.abs(dx), Math.abs(dy));

  var stepX = dx / length;
  var stepY = dy / length;

  var dist = 0;

  while (dist <= length) {
    drawPixel(Math.round(x1), Math.round(y1));
    x1 += stepX;
    y1 += stepY;
    dist += 1;
  }
}


function drawLineDashed(x1, y1, x2, y2) {
  // basic version: hardcoded, 5 pixel dash, 5 pixel gap
  // bonus point: use an array to define the dash pattern (dash, gap, ...), e.g.
  // var pattern = [8, 3, 2, 3];
  // bonus point: experiment with anti-aliasing, using drawPixel(x, y, color)

  var dx = x2 - x1;
  var dy = y2 - y1;

  var length = Math.max(Math.abs(dx), Math.abs(dy));

  var stepX = dx / length;
  var stepY = dy / length;

  var dist = 0;

  // var pattern = [5, 5, 10, 5]; // [dash, gap, ...]
  var patternBool = [];

  for (var i = 0; i < pattern.length; i++) {
    for (var k = 0; k < pattern[i]; k++) {
      if (i % 2 === 0) { // is even
        patternBool.push(true);
      } else {
        patternBool.push(false);
      }
    }
  }

  // repeat pattern until it matches length
  while (patternBool.length < length) {
    patternBool = patternBool.concat(patternBool);
  }

  while (dist <= length) {
    if (patternBool[dist]) { // only drawPixel if dist between 0-5, 10-15, etc.
      drawPixel(Math.round(x1), Math.round(y1));
    }

    x1 += stepX;
    y1 += stepY;
    dist += 1;
  }
}

/**
 * Function to return a rgb(r,g,b) string 
 * @param {number} dist number between 0-1
 */
function rgbString(dist) {
  dist *= 255;
  var distString = String(dist);
  return 'rgb(' + distString + ', ' + distString + ', ' + distString + ')';
}

function drawLineAntiAliasing(x1, y1, x2, y2) {
  var dx = x2 - x1;
  var dy = y2 - y1;

  var length = Math.max(Math.abs(dx), Math.abs(dy));

  var stepX = dx / length;
  var stepY = dy / length;

  var dist = 0;

  var distXbiggerY = Math.abs(dx) > Math.abs(dy);

  while (dist <= length) {
    var yUp = Math.ceil(y1);
    var distyUp = yUp - y1;
    var yDown = Math.floor(y1);
    var distyDown = y1 - yDown;

    var xUp = Math.ceil(x1);
    var distxUp = xUp - x1;
    var xDown = Math.floor(x1);
    var distxDown = x1 - xDown;


    var rgbyUp = rgbString(distyUp);
    var rgbyDown = rgbString(distyDown);

    var rgbxUp = rgbString(distxUp);
    var rgbxDown = rgbString(distxDown);


    if (distXbiggerY) { // if dx is bigger than dy
      drawPixel(Math.round(x1), yUp, rgbyUp);
      drawPixel(Math.round(x1), yDown, rgbyDown);
    } else {
      drawPixel(xUp, Math.round(y1), rgbxUp);
      drawPixel(xDown, Math.round(y1), rgbxDown);
    }

    x1 += stepX;
    y1 += stepY;
    dist += 1;

  }
}
