
// if you want to add another line drawing function, 
// add its name to this array and it will be drawn
var lineFunctions = ["drawLine", "drawLineDashed"];


function drawLine(x1, y1, x2, y2) {

    // TODO: remove dummy code and implement drawing a line

    // bonus point: experiment with anti-aliasing, using drawPixel(x, y, color)

    drawPixel(x1, y1);
    drawPixel(x2, y2);

}


function drawLineDashed(x1, y1, x2, y2) {

    // TODO: remove dummy code and implement drawing a dashed line

    // basic version: hardcoded, 5 pixel dash, 5 pixel gap
    // bonus point: use an array to define the dash pattern (dash, gap, ...), e.g.
    // var pattern = [8, 3, 2, 3];
    // bonus point: experiment with anti-aliasing, using drawPixel(x, y, color)

    drawPixel(x1, y1);
    drawPixel(x2, y2);

}

