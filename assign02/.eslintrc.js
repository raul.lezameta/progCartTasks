module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    "no-var": 0,
    "no-param-reassign": 0,
    "vars-on-top": 0,
    "no-console": 0,
    // "eqeqeq": 0,
    "no-plusplus": 0,
    "no-alert": 0,
    "prefer-template": 0,
    "max-len": 0,

    // "no-undef": 0,
    // "no-unused-vars": 0
  },
};
