# Assignment 2 - “Implement line drawing styles”

Using the provided fixture (ZIP file in TUWEL), implement different line drawing styles as covered in lecture 3. The file `linestyles.html` provides an environment to test your functions in the browser – for each line drawing function, a polyline will be rendered to a separate canvas using coordinates defined by mouse clicks.
In the file `linestyles.js`, implement the following functions to draw styled lines, using the provided global functions `fillPolygon()` and `drawQuadraticBezier()` for drawing (see next page for documentation):

* Implement a function `drawLineDashed(p1,p2)` that draws a dashed line segment from p1 to p2 using the geometry-based approach presented in lecture 3.
* Implement a function `drawLineArrows(p1,p2)` that draws a line segment with an “arrow” pattern (triangles pointing from p1 to p2, or more advanced arrow shapes).
* Implement a function `drawLinePencil(p1,p2)` that draws a line segment in a “sketchy” style, resembling a line sketched with a sharp pencil.
* Implement a function `drawLineMarker(p1,p2)` that

## You are allowed to:
* Use, add or change helper functions (e.g. vector math) at the top of the file as needed.

## You are not allowed to:
* Use any external functions other than the ones listed on page 2.
* Change any code outside the file linestyles.js.
* Copy code from anywhere. All code has to be typed in by you.

## Bonus Points Ideas:
* [x] Improve the arrangement of dashes/arrows and gaps along the line, with respect to a nicer distribution at line endings and corners.
* [ ] Make the shape of the arrow pattern vary over the length of the line (e.g. arrows getting larger)
* [ ] Improve the rendering of corners (line joins) for the dashed line. Each `drawLine*` function receives an additional 2 parameters: change the function to `drawLineDashed(p1,p2,previousPoint,nextPoint)` to access the previous and next points in the polyline, if available (these will be `null` for the first and last lines, respectively). You can use that information to figure out the appropriate geometry of the corner.
* [x] Make the orientation of the curve in `drawLinePencil` consistently match the expected drawing style of a right-handed person.
* [ ] … you can also come up with your own creative ideas or additional line drawing functions.
Grading will be based on the script providing a correct implementation and meeting the above criteria. You need to understand the code you submit and may be asked about its details at a future lab interview.