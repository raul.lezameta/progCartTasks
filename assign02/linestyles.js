// if you implement additonal line drawing functions, add their name here to have them rendered
var lineDrawingFunctions = [
  'drawLineDashed',
  'drawLineArrows',
  'drawLineArrowsLength',
  'drawLineMarker',
  'drawLinePencil',
  'drawLinePencilRightHand',
];

// you can change the number of lines that are kept here
var NUM_LINES = 5;

// *****************************************************************************
// Helper functions
// *****************************************************************************

// add two 2D vectors
function vecAdd(vec1, vec2) {
  return [vec1[0] + vec2[0], vec1[1] + vec2[1]];
}

// multiply 2D vector with scalar
function vecMult(vec, scalar) {
  return [vec[0] * scalar, vec[1] * scalar];
}

// displace a 2D point by a gaussian-distributed random offset
function displacePoint(point, radius) {
  radius = radius || 5;
  // randomGaussian is defined in fixture.js
  return vecAdd(point, [randomGaussian() * radius, randomGaussian() * radius]);
}

// *****************************************************************************
// Additional Helper functions
// *****************************************************************************

/**
 * Calculate the distance between 2 points
 * @param {number[]} p1 first Point
 * @param {number[]} p2 second Point
 */
function dist(p1, p2) {
  var a = (p2[0] - p1[0]);
  var b = (p2[1] - p1[1]);
  return Math.sqrt(a * a + b * b);
}

/**
 * Calculate normal vector
 * @param {number[]} v
 */
function normalVector(v) {
  return [v[1], -v[0]];
}

/**
 * Calculates unit vector pointing from p1 to p2
 * @param {number[]} p1 start point
 * @param {number[]} p2 end point
 */
function unitVector(p1, p2) {
  var length = dist(p1, p2);
  return [(p2[0] - p1[0]) / length, (p2[1] - p1[1]) / length];
}

// *****************************************************************************
// Assignment 2: Implement Line Drawing Functions
// *****************************************************************************

// You can use the following global functions
// - fillPolygon(pointsArray, fillColor)
// - drawQuadraticBezier(p1, p2, controlPoint, color, width)
// - drawLine(p1, p2, color, width)
// to do your drawing

// For bonus points, you can implement more advanced rendering of corners
// by accessing the previous and next points in the polyline by changing
// any of the methods to e.g.:
// drawLineDashed(p1, p2, previousPoint, nextPoint)

// *****************************************************************************
// Dashed Line
// *****************************************************************************


function drawLineDashed(p1, p2) {
  var DASH_LENGTH = 20;
  var DASH_WIDTH = 10;
  var GAP_LENGTH = 20;

  var length = dist(p1, p2);

  var patternLength = DASH_LENGTH + GAP_LENGTH;
  var numDashes = Math.floor(length / patternLength);

  if ((length % patternLength) > DASH_LENGTH) {
    numDashes += 1;
  }

  var unitV = [(p2[0] - p1[0]) / length, (p2[1] - p1[1]) / length];
  var normalV = normalVector(unitV);

  var pos = p1;
  for (var i = 0; i < numDashes; i++) {
    var tp1 = vecAdd(pos, vecMult(normalV, DASH_WIDTH / 2));
    var tp2 = vecAdd(tp1, vecMult(unitV, DASH_LENGTH));
    var tp3 = vecAdd(tp2, vecMult(normalV, -DASH_WIDTH));
    var tp4 = vecAdd(tp3, vecMult(unitV, -DASH_LENGTH));

    fillPolygon([tp1, tp2, tp3, tp4], 'rgb(0,0,0)');

    pos = vecAdd(pos, vecMult(unitV, patternLength));
  }
}

// *****************************************************************************
// Arrow Line
// *****************************************************************************

function drawLineArrows(p1, p2) {
  // arrow pattern parameters (you can adjust these to your liking)
  var ARROW_LENGTH = 20;
  var ARROW_WIDTH = 10;
  var GAP_LENGTH = 20;

  var length = dist(p1, p2);
  var patternLength = ARROW_LENGTH + GAP_LENGTH;

  var numArrows = Math.floor(length / patternLength);

  if ((length % patternLength) > ARROW_LENGTH) {
    numArrows += 1;
  }


  var unitV = unitVector(p1, p2);
  var normalV = normalVector(unitV);

  var arrowBack = p1;
  for (var i = 0; i < numArrows; i++) {
    var ap1 = vecAdd(arrowBack, vecMult(normalV, ARROW_WIDTH / 2));
    var ap2 = vecAdd(arrowBack, vecMult(normalV, -ARROW_WIDTH / 2));
    var arrowFront = vecAdd(arrowBack, vecMult(unitV, ARROW_LENGTH));

    fillPolygon([ap1, ap2, arrowFront], 'rgb(0,0,0)');

    arrowBack = vecAdd(arrowBack, vecMult(unitV, patternLength));
  }
}

function drawLineArrowsLength(p1, p2) {
  var length = dist(p1, p2);

  var ARROW_LENGTH = length / 10;
  var GAP_LENGTH = ARROW_LENGTH;
  var ARROW_WIDTH = 10;

  var patternLength = ARROW_LENGTH + GAP_LENGTH;

  var numArrows = Math.floor(length / patternLength);

  var unitV = unitVector(p1, p2);
  var normalV = normalVector(unitV);

  var arrowBack = p1;
  for (var i = 0; i < numArrows; i++) {
    var ap1 = vecAdd(arrowBack, vecMult(normalV, ARROW_WIDTH / 2));
    var ap2 = vecAdd(arrowBack, vecMult(normalV, -ARROW_WIDTH / 2));
    var arrowFront = vecAdd(arrowBack, vecMult(unitV, ARROW_LENGTH));

    fillPolygon([ap1, ap2, arrowFront], 'rgb(0,0,0)');

    arrowBack = vecAdd(arrowBack, vecMult(unitV, patternLength));
  }
}

// *****************************************************************************
// Sketchy Line: Pencil
// *****************************************************************************

function drawLinePencil(p1, p2) {
  var maxEr = 5;

  var length = dist(p1, p2);
  var unitV = unitVector(p1, p2);

  var cntrlPt = displacePoint(vecAdd(p1, vecMult(unitV, (length / 2) * Math.random() + length / 4)), length / 4);

  var nLines = 5;
  for (let i = 0; i < nLines; i++) {
    var cntrlPtPt = displacePoint(cntrlPt, length / 15);

    var tp1 = displacePoint(p1, maxEr);
    var tp2 = displacePoint(p2, maxEr);

    drawQuadraticBezier(tp1, tp2, cntrlPtPt, 'rgba(80, 80, 80, 0.3)');
  }
}

function drawLinePencilRightHand(p1, p2) {
  var maxEr = 5;

  var length = dist(p1, p2);

  var cntrlPt = [(p1[0] + p2[0]) / 2 - length / 15, (p1[1] + p2[1]) / 2 - length / 15];
  cntrlPt = displacePoint(cntrlPt, 10);

  var nLines = 5;
  for (let i = 0; i < nLines; i++) {
    var cntrlPtPt = displacePoint(cntrlPt, length / 15);

    var tp1 = displacePoint(p1, maxEr);
    var tp2 = displacePoint(p2, maxEr);

    drawQuadraticBezier(tp1, tp2, cntrlPtPt, 'rgba(80, 80, 80, 0.3)');
  }
}

// *****************************************************************************
// Sketchy Line: Marker Pen
// *****************************************************************************

function drawLineMarker(p1, p2) {
  var maxEr = 10;

  var length = dist(p1, p2);
  var unitV = unitVector(p1, p2);

  var nLines = 4;
  for (let i = 0; i < nLines; i++) {
    var tp1 = displacePoint(p1, maxEr);
    var tp2 = displacePoint(p2, maxEr);
    var cntrlPt = displacePoint(vecAdd(tp1, vecMult(unitV, length * Math.random())), length / 5);

    drawQuadraticBezier(tp1, tp2, cntrlPt, 'rgba(0,0,0,0.8)', 5);
  }
}
